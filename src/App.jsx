import React from 'react'
import './App.css'
import MainContent from './components/MainContent/MainContent';
import Navbar from './components/Navbar/Navbar.jsx';

function App() {

  return (
    <div className="App">
      <Navbar />
      <MainContent />
    </div>
  )
}

export default App
