import React from 'react'
import './style.css'

export default function Navbar(){
    return(
        <div className="nav">
            <img src="images/trollFace.png" alt="" />
            <h1>Meme Generator</h1>
            <p>React Course - Project 3</p>
        </div>
        
    )   
}