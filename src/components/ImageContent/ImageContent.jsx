import React from 'react'
import './style.css'

export default function ImageContent(props){
    const {randomImage, topText, bottomText} = props;
    return(
        <>
        <div className="image-container">
            <p className='top-text'>{topText}</p>
            <img src={randomImage} className="meme" />
            <p className='bottom-text'>{bottomText}</p>
        </div>
        

        </>
        
    )
}