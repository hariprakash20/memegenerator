import React from 'react'
import ImageContent from '../ImageContent/ImageContent'
import './style.css'

export default function MainContent(){

    const [meme, setMeme] = React.useState({
        topText:"",
        bottomText:"",
        randomImageUrl:"https:\/\/i.imgflip.com\/2bqzyl.jpg"
    })

    const [allMemes, setAllMemes] =React.useState([])

    React.useEffect(()=>{
        async function getAllMemes(){
            const response = await fetch("https://api.imgflip.com/get_memes")
            const memesData = await response.json();
            setAllMemes(memesData.data.memes);
        }
        getAllMemes();
    },[]);

    function changeMeme(event){
        event.preventDefault()
        let randomIndex = Math.floor(Math.random()* allMemes.length);
        setMeme(prevMeme => ({
            ...prevMeme,
            randomImageUrl:allMemes[randomIndex].url
        }))
    }

    function handleChange(event){
        event.preventDefault()
        setMeme(prevMeme => ({
           ...prevMeme, 
           [event.target.name]: event.target.value
        }))
    }


    return(
        <div className="mainContainer">
            <form className="form" onSubmit={changeMeme}>
                <div className="inputs">
                <input className='textBox' type="text" name="topText" value={meme.topText} onChange={handleChange} placeholder='top text'/>
                <input className='textBox' type="text" name="bottomText" value={meme.bottomText} onChange={handleChange} placeholder='bottom text'/>
                </div>
            <button className="newImagebtn" type="submit">Get a new meme image🖼</button>
            </form>
            <div className="imgContainer" ></div>
            <ImageContent randomImage={meme.randomImageUrl} topText={meme.topText} bottomText={meme.bottomText}/>
        </div>
    )
}
